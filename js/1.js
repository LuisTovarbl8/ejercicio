//variables
const aliceElement0=document.querySelector('#alice0');
const aliceElement1=document.querySelector('#alice1');
const aliceElement2=document.querySelector('#alice2');

const bobElement0=document.querySelector('#bob0');
const bobElement1=document.querySelector('#bob1');
const bobElement2=document.querySelector('#bob2');

const formulario = document.querySelector('#triplete');
const contenedor = document.querySelector('#resultados');
const btnEnviar = document.querySelector('#limpiador');

eventListeners();
function eventListeners(){
    document.addEventListener('DOMContentLoaded', iniciarApp);

    //campos del formulario
    aliceElement0.addEventListener('blur', validarFormulario);//cuando dejas un input 
    aliceElement1.addEventListener('blur', validarFormulario);//cuando dejas un input 
    aliceElement2.addEventListener('blur', validarFormulario);//cuando dejas un input 

    bobElement0.addEventListener('blur', validarFormulario);//cuando dejas un input 
    bobElement1.addEventListener('blur', validarFormulario);//cuando dejas un input 
    bobElement2.addEventListener('blur', validarFormulario);//cuando dejas un input 
}
function iniciarApp(){
    btnEnviar.disabled =  true;
    btnEnviar.classList.add('cursor-not-allowed', 'opacity-50');
}

function validarFormulario(e){
    if(e.target.value.length >0 ){
        //eliminar los arrores
        const error = document.querySelector('p.error');
        if(error){
            error.remove();
        }
        e.target.classList.remove('border', 'border-red-500');
        e.target.classList.add('border', 'border-green-500');
    }else{
        e.target.classList.remove('border', 'border-green-500');
        e.target.classList.add('border', 'border-red-500');
        mostrarError('Todos los campos son obligatorios');
    }

    if(aliceElement0.value!== '' && aliceElement1.value !== '' && aliceElement2.value !== '' && bobElement0.value !== ''  && bobElement1.value !== '' && bobElement2.value !== ''){
        analizarDatos();
     }
    

function mostrarError(mensajeror){
    const mensajeError = document.createElement('P');
    mensajeError.textContent=mensajeror;
    mensajeError.classList.add('border', 'border-red-500','bg-red-500' ,'background-red-100', 'text-black-500', 'p-3', 'mt-5', 'text-center', 'error');
    
    const errores = document.querySelectorAll('.error')
    if(errores.length ===0){
        formulario.appendChild(mensajeError);

    }
}

function analizarDatos(){

    let empate=0;
    let alice=0;
    let bob=0;
    let a=[Number(aliceElement0.value),Number(aliceElement1.value),Number(aliceElement2.value)];
    let b=[Number(bobElement0.value),Number(bobElement1.value),Number(bobElement2.value)];
    console.log(a[0],a[1],a[2]);
    console.log(b[0],b[1],b[2]);
    if(a[0]==b[0] || a[1]==b[1] || a[2]==b[2] ){
        empate++;
    }
    if(a[0]>b[0]){
        alice++;
    }
    if(b[0]>a[0]){
        bob++;
    }
    if(a[1]>b[1]){
        alice++;
    }
    if(b[1]>a[1]){
        bob++;
    }
    if(a[2]>b[2]){
        alice++;
    }
    if(b[2]>a[2]){
        bob++;
    }
    if(a[0]>=1 && a[0]<=100 && a[1]>=1 && a[1]<=100 && a[2]>=1 && a[2]<=100 && b[0]>=1 && b[0]<=100 && b[1]>=1 && b[1]<=100 && b[2]>=1 && b[2]<=100){
        const divNuevo= document.createElement('div');
        divNuevo.classList.add('cita', 'p-3');
        divNuevo.innerHTML= `Valores colocados por Alice:[ ${a} ] <br> Valores colocados por Bob: [ ${b} ]  <br>La jugadora Alice tiene: ${alice} puntos <br>La jugadora Bob tiene: ${bob} puntos <br> Matriz de entorno: [${alice } ${ bob}] <hr size="2px" color="black">`;
        btnEnviar.disabled =  false;
        btnEnviar.classList.remove('cursor-not-allowed', 'opacity-50');


        contenedor.appendChild(divNuevo);
        formulario.reset();
    }else{
        mostrarError('Revisa bien tus valores, Recuerda que solo puedes poner numeros que van desde 1 al 100 cualquier otro dato sera invalidado');
    }

}
}